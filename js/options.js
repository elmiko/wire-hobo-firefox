(function() {
  // add a listener for the submit event to do server comms
  document.getElementById("settings-form").addEventListener("submit", function(event) {
    var form = {
      server: document.getElementById("server").value,
      apikey: document.getElementById("apikey").value
    };
    console.log(form);
    if (form.server != '' && form.apikey != '') {
      lib.saveConfig(form)
    }
    event.preventDefault()
  })

  // set the initial values for the form
  lib.getConfig(function(items) {
    if (items.server != null) {
      document.getElementById("server").value = items.server
    }
    if (items.apikey != null) {
      document.getElementById("apikey").value = items.apikey
    }
  })
})()
