(function() {
  // create an alert
  function generateAlert(msg, severity) {
    var severityclass = "alert-" + severity
    var alerts = document.getElementById("alerts")
    var html = "<div id=\"alert-box\"><button type=\"button\" id=\"alert-close\"><span>&times;</span></button>" + msg + "</div>" + alerts.innerHTML
    alerts.innerHTML = html
    document.getElementById("alert-close").addEventListener("click", function(event) {
      event.preventDefault();
      alerts.innerHTML = ""
      window.close()
    })
  }

  // configure the new camp form and attach a callback that will post to the
  // server if everything looks good
  document.getElementById("new-camp-form").addEventListener("submit", function(event) {
    lib.getConfig(function(items) {
      if (items.server != null && items.apikey != null) {
        var signs = document.getElementById("signs").value.split(",").map(i => i.trim())
        var title = document.getElementById("title").value
        lib.getCurrentInfo(function(info) {
          var data = {
            title: title,
            url: info.url,
            signs: signs
          };
          var url = items.server + "/camps";
          var request = {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              "wire-hobo-apikey": items.apikey
            },
            body: JSON.stringify(data)
          }
          const response = fetch(url, request)
            .then(res => {
              if (res.ok) {
                window.close()
              } else {
                generateAlert("Error creating record, possible duplicate.")
              }
            })
            .catch((error) => {
              generateAlert("Error during save", "danger")
            })
        })
      } else {
        generateAlert("No server or apikey.", "danger")
      }
    })
    event.preventDefault()
  })

  document.getElementById("connect-button").addEventListener("click", function(event) {
    lib.getConfig(function(items) {
      if (items.server != null) {
        browser.tabs.create({
          url: items.server
        });
        window.close();
      } else {
        generateAlert("No server.", "danger")
      }
    })
    event.preventDefault()
  })

  // put the title and url in the popup display
  lib.getCurrentInfo(function(info) {
    document.getElementById("title").value = info.title
  });
})()
