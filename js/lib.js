var lib = {
  getConfig: function (callback) {
    // get the server config information and pass it to the callback function
    var getStorage = browser.storage.local.get({
      server: null,
      apikey: null
    });
    getStorage.then(function(items) {
      return callback(items);
    });
  },

  getCurrentInfo: function (callback) {
  // get the url and title info from the current tab in the browser
    var info = {
      title: null,
      url: null
    };
    var getCurrent = browser.windows.getCurrent();
    getCurrent.then(function(currentWindow) {
      var tabsQuery = browser.tabs.query({active: true, windowId: currentWindow.id});
      tabsQuery.then(function(activeTabs) {
          info.title = activeTabs[0].title;
          info.url = activeTabs[0].url;
          callback(info);
        });
    });
  },

  saveConfig: function (conf) {
    // save the server configuration values in the chrome local storage
    browser.storage.local.set({
      server: conf.server,
      apikey: conf.apikey
    });
  },
}
