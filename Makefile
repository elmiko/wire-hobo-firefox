.PHONY: clean

all: wire-hobo-firefox.zip

wire-hobo-firefox.zip:
	zip -r wire-hobo-firefox.zip * -x Makefile

clean:
	rm wire-hobo-firefox.zip
